from parse_audio import *
from keras.models import load_model


test_dir = os.path.join(os.getcwd(), "Test-Songs")
model_name = input("Enter model name: ")
model = load_model(os.path.join("Models", model_name + ".h5"))
genres = []
raw_results = []

#Gets genre from model output
def get_genre(model_output):
    genre_index = np.array(model_output).argmax()
    return genres[genre_index]

for path, directories, files in os.walk(test_dir): #Loop through all genres and songs
    for genre_dir in directories:
        genres.append(genre_dir)
        for inner_path, inner_dirs, songs in os.walk(os.path.join(test_dir, genre_dir)):
            for song in songs:
                file_path = os.path.join(test_dir, genre_dir, song)
                parsed = parse(file_path)
                print(parsed)
                model_output = model.predict(np.array([parsed]))
                raw_results.append([song, genre_dir, model_output])

for result in raw_results: #Replace model output with genre string
    result[-1] = get_genre(result[-1])

bool_results = []
for result in raw_results: #Check how many correct results
    bool_results.append(result[1] == result[2])

successful = bool_results.count(True)
total = len(bool_results)
print("Successful:", successful)
print("Total:", total)
print("Ratio:", successful / total)


