import os

data_dir = os.path.join(os.getcwd(), "Training-Songs")
print("---------Song Downloader---------")
playlist = input("Enter youtube playlist link: ") #Link must only include 'list=' and not 'v='
genre = input("Enter genre: ") #Should match a subfolder of 'Songs\'

ydl_exe = os.path.join('Executables', 'youtube-dl.exe')
output_format = os.path.join(data_dir, genre, "%(title)s.%(ext)s")
print(output_format)
"""
Flags:
    -i: Ignores "Unavailable in your area" or removed videos
    -o: Sets the output directory 'data_dir\genre'
"""
os.system(
    f"{ydl_exe} -i -o \"{output_format}\" --extract-audio --audio-format mp3 {playlist}")



