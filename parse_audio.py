import librosa
import os
import numpy as np

data_dir = os.path.join(os.getcwd(), "Training-Songs")
print(data_dir)

"""
Parses an audio file and returns only the features
"""
def parse(absolute_path):
    try:
        # here kaiser_fast is a technique used for faster extraction
        X, sample_rate = librosa.load(absolute_path, res_type='kaiser_fast')
        # we extract mfcc feature from data
        mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
    except Exception as e:
        print("Error encountered while parsing file: ", absolute_path)
        return None

    feature = mfccs
    return feature

"""
Uses parse() and appends a label
"""
def parse_by(genre, song_file):
    file_name = os.path.join(data_dir, genre, song_file)
    feature = parse(file_name)
    label = genre
    return [feature, label]
