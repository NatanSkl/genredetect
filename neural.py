from parse_audio import *
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import Adam
from keras.utils import np_utils
from sklearn import metrics

#Contains all song data
df = pd.DataFrame(columns=["feature", "label"])

#Loop through all songs and parse them using code from parse_audio.py

for path, directories, files in os.walk(data_dir): #loop through genre directories
    for dir in directories:
        for inner_path, inner_directories, songs in os.walk(os.path.join(path, dir)): #loop through songs
            for song_file in songs:
                genre = dir
                result = parse_by(genre, song_file)
                print(result)
                df = df.append(
                    {"feature": result[0], "label": result[1]}, ignore_index=True) #append to DataFrame

#Preprocess all the data to pass to our deep learning model

X = np.array(df["feature"].tolist())
y = np.array(df["label"].tolist())
lb = LabelEncoder() #Preprocessing component used to organizes the genre labels
y = np_utils.to_categorical(lb.fit_transform(y))

print(X)
print(y)

#Build neural model

num_labels = y.shape[1]
filter_size = 2

model = Sequential()

model.add(Dense(256, input_shape=(40,)))
model.add(Activation("relu"))
model.add(Dropout(0.5))

model.add(Dense(256))
model.add(Activation("relu"))
model.add(Dropout(0.5))

model.add(Dense(num_labels))
model.add(Activation("softmax"))

model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')

#Actual training

model.fit(X, y, batch_size=10, epochs=5, validation_split=0.2)

#print("Test for pop song: ")
#print(model.predict(parse(r"C:\Users\magshimim\Documents\GenreDetect\Kesha - Tik Tok.mp3"), verbose=1))
#exec(open("./path/to/script.py").read(), globals())



